PROJECT_NAME := "LegionnaireGo"
PKG := "gitlab.com/legion-robotnik/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)

.PHONY: all dep vet build build-macos build-linux format clean test coverage

all: dep vet clean coverage build build-macos build-linux

format: ## Format the files
	@go fmt $(PKG_LIST)

vet: format ## Vet the files
	@go vet -v $(PKG_LIST)

dep: ## Get the dependencies
	@go get -v -d ./...

race: dep ## Run tests and check for race conditions
	@go test -race $(PKG_LIST)

coverage_dir:
	@mkdir -p .coverage/

target_dir:
	@mkdir -p target/darwin-arm64/
	@mkdir -p target/linux-amd64/

test: race coverage_dir ## Run tests and output coverage report
	@go test -v -cover -coverprofile=.coverage/$(PROJECT_NAME).cov $(PKG_LIST)

coverage: test ## Generate global code coverage report in HTML
	@go tool cover -o .coverage/index.html -html=.coverage/$(PROJECT_NAME).cov;

build: dep target_dir ## Build the binary file for current OS
	@go build -o target/ -v $(PKG_LIST)

build-macos: dep target_dir ## Build the binary file for MacOS/arm64
	@env GOOS=darwin GOARCH=arm64 go build -o target/darwin-arm64/ -v $(PKG_LIST)

build-linux: dep target_dir ## Build the binary file for Linux/amd64
	@env GOOS=linux GOARCH=amd64 go build -o target/linux-amd64/ -v $(PKG_LIST)

clean: ## Remove previous build
	@rm -f target/linux-amd64/*
	@rm -f target/darwin-arm64/*

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
