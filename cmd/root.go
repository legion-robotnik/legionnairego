/*
Copyright © 2023 Eugene Kovalev euge.kovalev@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"fmt"
	"gitlab.com/legion-robotnik/LegionnaireGo/files"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use: "legionnaire",
	Args: func(cmd *cobra.Command, args []string) error {
		if err := cobra.MinimumNArgs(1)(cmd, args); err != nil {
			return err
		}
		// Check that the argument is a valid directory
		if files.ValidDirPath(args[0]) {
			return nil
		}
		return fmt.Errorf("Invalid directory path provided: %s", args[0])
	},
	Short: "A monitoring and alerting agent for the Legion ecosystem",
	Long: `A monitoring and alerting agent for the Legion ecosystem. Given a directory to monitor that follows a structure like the one below:
legion.d
├── config.yaml <- legionnaire config
├── my-reporter.d
│   ├── env
│   │   └── ... ← virtual env for reporter, managed by legionnaire
│   ├── etc
│   │   └── config.yaml ← configuration for reporter, fed into reporter on STDIN
│   ├── legion.yaml ← tells legionnaire how to run the reporter
│   └── src
│       ├── reporter.py ← reporter source code
│       └── requirements.txt ← reporter's required libraries, installed in dedicated env'
└── system.d ← another reporter
    ├── env
    │   └── ...
    ├── etc
    │   └── config.yaml
    ├── legion.yaml
    └── src
        ├── reporter.py
        └── requirements.txt

This agent will run all the properly configured reporters according to their settings. Critically, it will monitor the directory tree for changes and reload reporters when changes are detected. For more information on how to define and configure reporters, see https://gitlab.com/legion-robotnik/legionnairego/-/blob/main/README.md
`,
	Run: func(cmd *cobra.Command, args []string) {
		// Do something here...
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.LegionnaireGo.yaml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".LegionnaireGo" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigType("yaml")
		viper.SetConfigName(".LegionnaireGo")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		_, err := fmt.Fprintln(os.Stderr, "Using config file:", viper.ConfigFileUsed())
		if err != nil {
			return
		}
	}
}
