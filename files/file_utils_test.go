package files

import (
	"github.com/chigopher/pathlib"
	"github.com/matryer/is"
	"testing"
)

func tempPath(t *testing.T) *pathlib.Path {
	return pathlib.NewPath(t.TempDir())
}

func TestValidDirPath(t *testing.T) {
	Is := is.New(t)
	tmpDir := t.TempDir()
	Is.True(ValidDirPath(tmpDir))
}

func TestInvalidDirPath(t *testing.T) {
	Is := is.New(t)
	invalidDir := tempPath(t).Join("does-not-exist")
	Is.True(!ValidDirPath(invalidDir.String()))
}
