package files

import "github.com/chigopher/pathlib"

func ValidDirPath(candidate string) bool {
	path := pathlib.NewPath(candidate)
	pathIsDir, err := path.IsDir()
	return pathIsDir && err == nil
}
