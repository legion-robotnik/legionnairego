# LegionnaireGo

An alerting and monitoring agent which runs reporters and publishes information to the Legion eco-system.

## Deployment

You can always deploy the latest binary of the agent by downloading the latest artifact using the GitLab Artifacts API:

For MacOS (arm64):
```bash
wget https://gitlab.com/api/v4/projects/51037821/jobs/artifacts/main/raw/target/darwin-arm64/LegionnaireGo?job=compile -O legionnaire
```

For Linux (amd64):
```bash
wget https://gitlab.com/api/v4/projects/51037821/jobs/artifacts/main/raw/target/linux-amd64/LegionnaireGo?job=compile -O legionnaire
```