package main

import "testing"

func TestHello(t *testing.T) {
	expected := "Hello, world!"
	got := Hello()
	if got != expected {
		t.Errorf("Got '%s' instead of '%s'", got, expected)
	}
}
